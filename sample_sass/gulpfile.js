var gulp = require('gulp')
    sass = require('gulp-sass')
    sourcemaps = require('gulp-sourcemaps')
    del = require('del')
    autoprefixer = require('gulp-autoprefixer')
    concat = require('gulp-concat')
    browserSync = require('browser-sync');

var srcPaths = {
    sass: './sass/**/*.scss'
}
var distPaths = {
    sass: './css'
}

gulp.task('clean', function (cb) {
    del(distPaths.sass, cb);
    cb();
});
gulp.task('reload', function () {
    return gulp.src('./')
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('sass', function () {
    return gulp.src(srcPaths.sass)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(concat('styles.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(distPaths.sass))
        .pipe(browserSync.reload({stream:true}));
});
gulp.task('watch', function () {
    gulp.watch(srcPaths.sass, gulp.series('sass'));
    gulp.watch("./*.html", gulp.series('reload'));
});
gulp.task('serve', function(cb) {
    browserSync({
        server: {
            baseDir: "./"
        },
        port: 4000,
        notify: true,
        open: true
    }, cb);
});

gulp.task('all', gulp.parallel('sass'));
gulp.task('build', gulp.series('clean', 'all'));

gulp.task('default', gulp.series('build', 'serve', 'watch'));